# Git-Test

Ce répertoire sert de bac à sable pour l'apprentissage du HTML/CSS/JS de l'application OrthoBeatbox.

## À propos du projet

Le projet Ortho-Beatbox a été conçue suite aux différents mémoires d'orthophonie menés autour du Human Beatbox et de l'orthophonie, encadrés par Nathalie Henrich, chercheuse au CNRS de Grenoble.

## Contributeurs

- Vanessa
- Jonathan
